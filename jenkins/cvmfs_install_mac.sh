#!/bin/bash -x
# Using the following environment variables:
#  BUILDMODE: 'nightly' or 'release' 
#  LCG_VERSION: Stack version
#  PLATFORM: Platform
#  WORKSPACE: Jenkins workspace

REPOSITORY='sft-nightlies-test.cern.ch'
exit_code=0

weekday=`date +%a`
if [[ "${BUILDMODE}" == "nightly" ]]; then
  cvmfs_user=cvsft-nightlies-test
else
  cvmfs_user=cvsft
fi
sudo -i -u ${cvmfs_user}<<EOF
shopt -s nocasematch
if [[ "${BUILDMODE}" == "nightly" ]]; then
  # -t 6000, retry for 6000 seconds
  cvmfs_server transaction -t 6000 ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}
else
  cvmfs_server transaction -t 6000 sft.cern.ch
fi
retVal=\$?
if [ "\$retVal" == "17" ]; then
   # return value 17 is transaction in progress (EEXIST)
   # After 100 minutes in nightly mode the opened transaction is forcibly aborted
   # Only during the night (between 7pm and 8am)
   H=`date +%H`
   if [[ "${BUILDMODE}" == "nightly" && (19 -le "\$H" || "\$H" -lt 8) ]]; then
       echo "Forcing transaction abortion"
       cvmfs_server abort -f s${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}
       cvmfs_server transaction s${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}
   else
       exit 1
   fi
elif [ "\$retVal" != "0" ]; then
   echo "Error starting transaction"
   exit 1
fi

echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
echo LCG_${LCG_VERSION}_$PLATFORM.txt
echo Weekday:   $weekday
echo BuildMode: $BUILDMODE
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

set -x

export COMPILER=${COMPILER}
export LCG_VERSION=${LCG_VERSION}
export weekday=${weekday}

if [[ "${BUILDMODE}" == "nightly" ]]; then
  cvmfs_rsync -a -e "ssh -o StrictHostKeyChecking=no" sftnight@${BUILDHOSTNAME}:/Users/Shared/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}/* /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}
  if [ "\$?" != "0" ]; then
    echo "There was an error installing packages, quitting"
    cvmfs_server abort ${REPOSITORY}
    exit 1
  fi

  $WORKSPACE/lcgcmake/jenkins/extract_LCG_summary.py /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM} $PLATFORM $LCG_VERSION RELEASE

  cvmfs_server publish ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}
  if [ "\$?" != "0" ]; then
    echo "There was an error publishing, quitting"
    cvmfs_server abort ${REPOSITORY}
    exit 1
  fi

  cvmfs_server transaction -t 6000 ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}

  cvmfs_rsync -a -e "ssh -o StrictHostKeyChecking=no" --del sftnight@${BUILDHOSTNAME}:/Users/Shared/cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}/* /cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}
  if [ "\$?" == "0" ]; then
    cvmfs_server publish ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}
    echo "View created, updating latest symlink"
  else
    echo "There was an error creating view, not updating latest symlink"
    cvmfs_server abort ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}
    exit_code=1
  fi
  cd $HOME
elif [[ "${BUILDMODE}" == "release" ]]; then
  cvmfs_rsync -a -e "ssh -o StrictHostKeyChecking=no" sftnight@${BUILDHOSTNAME}:/Users/Shared/cvmfs/sft.cern.ch/lcg/releases /cvmfs/sft.cern.ch/lcg
  cvmfs_rsync -a -e "ssh -o StrictHostKeyChecking=no" --del sftnight@${BUILDHOSTNAME}:/Users/Shared/cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}/$PLATFORM /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}
  $WORKSPACE/lcgcmake/jenkins/extract_LCG_summary.py /cvmfs/sft.cern.ch/lcg/releases/LCG_${LCG_VERSION} $PLATFORM $LCG_VERSION RELEASE
  cd $HOME
  cvmfs_server publish sft.cern.ch
fi
EOF
exit $exit_code
