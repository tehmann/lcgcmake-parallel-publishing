This file describes how to install generators to MCGenerators trees:

1. Download new authors tarfile to local source tarfile repository:

    a. Login as sftnight to lcgapp-slc6-physical1 or lcgapp-slc6-physical2;

    b. Download source tarball;

    c. Put the source tarball into EOS `xrdcp -f <tarball> root://eosuser.cern.ch//eos/project/l/lcg/www/lcgpackages/tarFiles/sources/MCGeneratorsTarFiles/`;

2. Add package definition to requested version of LCGCMT release: `cmake/toolchain/heptools-<release>.cmake` or, if present, to `cmake/toolchain/heptools-dev-generators.cmake`;

3. Try build on local node and run GENSER test for the given package;

4. Commit changes into git;

6. Go to https://epsft-jenkins.cern.ch/job/lcg_release_tar/
   or to the main Jenkins page https://epsft-jenkins.cern.ch/ and select lcg_release_tar.
   This requires permission (now it is done through the GENSER e-group).

7. Login to Jenkins. This requires a Jenkins account, to be created once (ask Patricia).

8. Select "Build with parameters" and build the package requested:


    | Parameter    | Value |
    | ------------ | -------------------------------------------- |
    | LCG_VERSION  | LCG release version (e.g. 87, ...) |
    | TARGET       | name of the top level package (e.g. `rivet-2.3.0`) |
    | BUILD_POLICY | `limited` |
    | LCG_INSTALL_PREFIX | `None` |
    | Combinations | select the necessary compilers for "slc6" or "centos7". Please pick "gcc62binutil" instead of "gcc62". For Ubuntu, pick "native". | 
    | VERSION_MAIN      | LCGCMake branch to use for the build (LCG_<LCG_VERSION>) | 
    | RPMS_CREATION     | Checked |
  
    Keep the rest of the parameters at their default values.

6. To install the packages, use [lcg_afs_install](https://epsft-jenkins.cern.ch/job/lcg_afs_install) and [lcg_cvmfs_install](https://epsft-jenkins.cern.ch/job/lcg_cvmfs_install) jobs with the following parameters:

    | Parameter    | Value |
    | ------------ | -------------------------------------------- |
    | LCG_VERSION  | LCG release version (e.g. 87, ...) |
    | VERSION_MAIN      | LCGCMake branch to use for the build (LCG_<LCG_VERSION>) | 
    | VERSION_JENKINS   | "master" |
    | PLATFORM | Full platform string "x86-64-(os)-(compiler)-(opt/dbg)" |
    | BUILDMODE| Always "release"  | 

    Keep the rest of the parameters at their default values.

7. Copy RPMs to the repository and update it. This is done by running  [complete_rpms_release_actions](https://epsft-jenkins.cern.ch/job/complete_rpms_release_actions) job with the following parameters:

    | Parameter    | Value |
    | ------------ | -------------------------------------------- |
    | LCG_VERSION  | LCG release version (e.g. 87, ...) |

    Keep the rest of the parameters at their default values.

8. If the installation is successfull, mail to genser-announce@cern.ch like this:

    > Dear colleagues,
    
    > New Tauola++ version - Tauola++ 1.1.4 has been installed into MCGenerators lcgcmt trees:

    >  \- MCGenerators_lcgcmt65

    >   \- MCGenerators_lcgcmt65a

    >   \- MCGenerators_lcgcmt66

    >   \---

    >   Best regards,

    >   GENSER
