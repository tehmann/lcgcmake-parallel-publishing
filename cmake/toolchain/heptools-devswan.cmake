#---List of externals
set(LCG_PYTHON_VERSION 3)
include(heptools-dev3)

LCG_external_package(java              11.0.21p9                                  )

LCG_external_package(c_ares            1.17.1                                   )
if(${LCG_OS}${LCG_OSVERS} MATCHES el9)
  LCG_external_package(ngbauth_submit    0.29-1                                   )
  LCG_external_package(myschedd          1.9-2                                    )
  LCG_external_package(condor            23.0.2                                   )
else()
  LCG_external_package(condor            8.9.11                                   )
  LCG_external_package(myschedd          1.7-2                                    )
  LCG_external_package(ngbauth_submit    0.25-2                                   )
endif()
LCG_external_package(linux_pam         1.5.1                                    )
LCG_external_package(GraalVM           22.0.0.2                                 )

# new packages for kfp
LCG_external_package(docstring_parser             0.15    )
LCG_external_package(fire                         0.4.0   )
LCG_external_package(google_api_core              2.10.1  )
LCG_external_package(google_api_python_client     1.12.11 )
LCG_external_package(googleapis_common_protos     1.56.4  )
LCG_external_package(kfp                          2.4.0   )
LCG_external_package(kfp_pipeline_spec            0.2.2   )
LCG_external_package(kfp_server_api               2.0.3   )
LCG_external_package(requests_toolbelt            0.9.1   )
LCG_external_package(strip_hints                  0.1.10  )
LCG_external_package(typer                        0.6.1   )
LCG_external_package(uritemplate                  3.0.1   )

#needs googleapis_common_protos
LCG_external_package(grpcio_status     1.56.2                                   )

# override versions for existing packages
LCG_external_package(cloudpickle       2.2.0                                    )
