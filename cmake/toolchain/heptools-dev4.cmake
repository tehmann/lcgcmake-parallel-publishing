#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT v6-30-00-patches GIT=http://root.cern.ch/git/root.git)
#LCG_external_package(ROOT 6.30.02)

# SPI-2482: Add Boost 1.84.0 for testing on dev4
if((${LCG_COMP} MATCHES gcc AND ${LCG_COMPVERS} GREATER 8) OR (${LCG_COMP} MATCHES clang AND ${LCG_COMPVERS} GREATER 12))
  LCG_external_package(Boost             1.84.0                                   )
else()
  LCG_external_package(Boost             1.78.0                                   )
endif()

#if(${LCG_OS}${LCG_OSVERS} MATCHES centos|ubuntu|el)
#  if(((${LCG_COMP} MATCHES gcc) AND (${LCG_COMPVERS} GREATER 7)) OR (${LCG_COMP} MATCHES clang))
#    LCG_AA_project(Gaudi  v36r14.testtbb GIT=https://gitlab.cern.ch/dkonst/Gaudi.git)
#  endif()
#endif()

#---Apple MacOS special removals and overwrites--------------------
include(heptools-macos)

if(LCG_ARCH MATCHES "aarch64")
  include(heptools-devARM)
endif()

