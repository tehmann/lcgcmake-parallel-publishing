#!/usr/bin/env python3

# Usage: $0 LCG_.*txt

import sys

filename = sys.argv[1]
print("Checking", filename)

lines = [x.strip() for x in open(filename, 'r').readlines()]

for line in lines:
    if line.startswith('#'):
        continue
    # check that there are no empty hashes
    blocks = line.split(', ')
    for block in blocks:
        key = block.split(':')[0].strip()
        value = block.split(':')[1].strip()
        if key == "HASH" and value == "":
            print("Found empty hash in line \n {0}\n".format(line))
            sys.exit(1)
        if key == "DEPENDS":
            for subpackage in value.split(","):
                if subpackage.endswith('-'):
                    print("Found empty hash in line: \n\n {0}\n\n Package: {1}".format(line, subpackage))
                    sys.exit(1)
